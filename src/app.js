export class App {
  configureRouter(config, router) {
    config.title = 'CMK2';
    config.map([
      {
        route: ["","home"],
        name: "Home",
        moduleId: "./home/home",
        title: "Modern Home",
        showSplash: true
      },
      {
        route: ["","product-details/:id"],
        name: "ProductDetails",
        moduleId: "./product/product-details",
        showSplash: true
      },
      {
        route: ["","shopping-cart"],
        name: "ShoppingCart",
        moduleId: "./order/shopping-cart",
        showSplash: true
      },
      {
        route: ["","login"],
        name: "Login",
        moduleId: "./login/login",
        showSplash: true
      },
      {
        route: ["","registration"],
        name: "Registration",
        moduleId: "./registration/registration",
        showSplash: true
      }
    ]);

    this.router = router;
  }
}
