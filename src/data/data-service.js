import {inject} from "aurelia-framework";
import {DataAPI} from "./../api/data-api";
import clientsConfig from "./../configs/clients";

@inject(DataAPI)
export class DataService {
	constructor(dataAPI) {
		this.dataAPI = dataAPI;
	}

	errToJSONOrFallback(err) {
		return new Promise((resolve, reject) => {
			err.json ? resolve(err.json()) : resolve(err);
		});
	}

	showToastr(msg) {
		toastr.options = {timeout: 10000};
		toastr.error(`${msg}`, "Server Error")
	}

	handleError(err) {
		this.errToJSONOrFallback(err).then(errData => this.showToastr(errData.message));
	}

  getProduct(id) {
    return this.dataAPI.get(`Products/${id}`)
  .catch(err => this.handleError(err));
  }
}
