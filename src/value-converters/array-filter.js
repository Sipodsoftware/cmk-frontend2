export class ArrayFilterValueConverter {
    toView(value, config) {
        // convention is that -1 is ALL items
        return value.filter(item => config.negation ? (item[config.propertyName] != config.propertyValue) : (item[config.propertyName] == config.propertyValue) || (config.propertyValue == -1))
    }
}