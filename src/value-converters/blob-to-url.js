import {inject} from "aurelia-framework";

import {TypeChecker} from "../tools/type-checker";
import {PrefixBase64ValueConverter} from "./prefix-base64";

@inject(TypeChecker, PrefixBase64ValueConverter)
export class BlobToUrlValueConverter {
	constructor(typeChecker, prefixBase64) {
		this.typeChecker = typeChecker;
		this.prefixBase64 = prefixBase64;
	}

	toView(blob) {
		if (blob) {
			if (this.typeChecker.isFile(blob)) {
				return URL.createObjectURL(blob);
			}

			return this.prefixBase64.toView(blob);
		}
	}
}