export class CurrencyFormatValueConverter {
	toView(val) {
		var newVal = parseInt(val, 10);

		if (newVal) {
			return newVal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
		}
	}
}