export class PrefixBase64ValueConverter {
	toView(base64String) {
		if (base64String) {
			if (base64String.charAt(0) === "/") {
				return `data:image/jpeg;base64,${base64String}`;
			} else if (base64String.charAt(0) === "i") {
				return `data:image/png;base64,${base64String}`;
			}
		}
	}
}