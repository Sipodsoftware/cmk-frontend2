import {inject} from "aurelia-framework";

import {TypeChecker} from "../tools/type-checker";

@inject(TypeChecker)
export class FileListToItemValueConverter {
	constructor(typeChecker) {
		this.typeChecker = typeChecker;
	}

	toView(fileList) {
		if (this.typeChecker.isFileList(fileList)) {
			return (fileList.length) ? fileList.item(0) : null;
		}
		
		return fileList;
	}
}