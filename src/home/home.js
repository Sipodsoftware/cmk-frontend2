import { inject } from 'aurelia-framework';
import { MdToastService } from './../../node_modules/aurelia-materialize-bridge/dist/commonjs/toast/toastService'

@inject(MdToastService)
export class Home {
  constructor(toast) {
    this.toast = toast;
  }

  showToast(e) {
    this.toast.show(`You clicked ${e.target.innerText}`, 4000);
  }
}

