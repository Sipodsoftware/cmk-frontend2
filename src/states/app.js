export class ApplicationState {
	constructor () {
		this.isNavigating = false;

		this.productTypeCategories = [];
		this.currentProductTypeCategory = null;

		this.productTypes = [];
		this.currentProductType = null;

		this.manufacturers = [];
		this.currentManufacturer = null;

		this.manufacturerModels = [];
		this.currentManufacturerModel = null;

		this.products = [];
		this.allProductsForCurrentProduct = [];
		this.currentProduct = null;

		this.productModels = [];
		this.appleModels = [];
		this.samsungModels = [];
		this.restModels = [];

		this.customizedProduct = {
			name: null,
			picture: null,
			productId: null,
			price: null
		};

		this.orders = [];
		this.currentOrder = null;

		this.order = {
			name: null,
			orderItems: []
		};

		this.orderItems = [];

		this.isNotOnOrderPage = false;
		this.isProductDesignerVisible = true;

		this.isPercentageCircle = false;
		this.percentageStatus = 0;
		this.continueShoppigPath = null;
		
		this.isOnAdminPanel = false;
		
		// all properties that are responsible for UI
		this.isNavBarVisible = null;
		this.isAdminNavBarVisible = null;
		this.bgWhite = null;
		this.bgRadialGrey = null;
		this.interceptWithLoadingSpinner = true;

		this.orderStatuses = ['sent', 'in progress', 'Ordered']
	}

	getManufacturerById(id) {
		return (manufacturer) => {
			return manufacturer.id === id;
		};
	}

	getManufacturerModelById(id) {
		return (manufacturerModel) => {
			return manufacturerModel.id === id;
		}
	}

	getProductTypeById(id) {
		return (productType) => {
			return productType.id === id;
		}
	}

	getProductTypeCategoryById(id) {
		return (productTypeCategory) => {
			return productTypeCategory.id === id;
		}
	}

	getProductById(id) {
		return (product) => {
			return product.id === id;
		}
	}

	getOrderById(id) {
		return (order) => {
			return order.id === id;
		}
	}

	showLoadingSpinner() {
		this.isPercentageCircle = true;
		this.percentageStatus = false;
	}

	hideLoadingSpinner() {
		this.isPercentageCircle = false;
		this.percentageStatus = 0;
	}

	enableInterceptLoadingSpinner() {
		this.interceptWithLoadingSpinner = true;
	}

	disableInterceptLoadingSpinner() {
		this.interceptWithLoadingSpinner = false;
	}
}