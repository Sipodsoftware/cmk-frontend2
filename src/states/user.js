import {inject} from "aurelia-framework";
import {Router} from "aurelia-router";

import CLAIMS from "./../states/claims";
import {AuthAPI} from "./../api/auth-api";

@inject(AuthAPI, Router)
export class UserState {
	constructor(authAPI, router) {
		this.authAPI = authAPI;
		this.router = router;

		// Used for checking if user is logged
		this.isLoggedIn = false;

		// Token we hold and send with every request
		this.token = null;

		// All user information
		this.user = null;

		this.CLAIMS = CLAIMS;
	}

	get isLogged() {
		return this.isLoggedIn;
	}
	
	login(formData) {
		return this.authAPI.login(formData);
	}

	logout() {
		this.isLoggedIn = false;
		this.token = null;
		this.user = null;

		this.router.navigate("modernHome");
	}

	isAuthenticated() {
		return this.user && Object.keys(this.user).length > 0;
	}

	hasClaim(claim) {
		return this.user.claims.indexOf(claim) !== -1;
	}
}