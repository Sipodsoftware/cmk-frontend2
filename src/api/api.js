export class API {
	handleResponse(response) {
		return new Promise((resolve, reject) => {
			response.json().then(result => {
				if (response.status >= 200 && response.status < 300) {
					resolve(result);
				} else {
					reject(result);
				}
			});
		});
	}

	handleAjaxResponse(response) {
		return new Promise((resolve, reject) => {
			if (response && response.status && response.status !== 200) {
				reject(response);
			} else {
				resolve(response);
			}
		});
	}
}