import {inject} from "aurelia-framework";

import {AuthHttpClient} from "../clients/auth-http-client";
import {API} from "./api";

@inject(AuthHttpClient)
export class AuthAPI extends API {
	constructor(authHttpClient) {
		super();
		this.authHttpClient = authHttpClient;
	}

	postRequest(url, data) {
		return this.authHttpClient.fetch(url, {method: "POST", body: $.param(data)})
			.then(res => this.handleResponse(res));
	}

	login(formData) {
		return this.postRequest("Users/Token", formData);
	}
}