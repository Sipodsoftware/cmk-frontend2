import {inject} from "aurelia-framework";

import {DataHttpClient} from "../clients/data-http-client";
import {API} from "./api";

@inject(DataHttpClient)
export class DataAPI extends API {
	constructor(dataHttpClient) {
		super();
		this.dataHttpClient = dataHttpClient;
	}

	get(url) {
		return this.dataHttpClient.fetch(url)
			.then(res => this.handleResponse(res));
	}
	
	post(url, data) {
		return this.dataHttpClient.fetch(url, {method: "POST", body: JSON.stringify(data)})
			.then(res => this.handleResponse(res));
	}

	postRaw(url, data) {
		return this.dataHttpClient.fetch(url, {method: "POST", body: data})
			.then(res => this.handleResponse(res));
	}

	remove(url) {
		return this.dataHttpClient.fetch(url, {method: "DELETE"});
	}

	download(url) {

	}
}