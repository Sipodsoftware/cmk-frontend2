import {inject} from "aurelia-framework";

import {API} from "./api";
import {AjaxClient} from "../clients/ajax-client";

@inject(AjaxClient)
export class AjaxAPI extends API {
	ajaxClient;
	constructor(ajaxClient) {
		super();
		this.ajaxClient = ajaxClient;
	}

	get(url, data) {
		return this.ajaxClient.fetch(url, {metgod: "GET", data})
			.then(res => this.handleAjaxResponse(res));
	}

	post(url, data, onProgressChange) {
		return this.ajaxClient.fetch(url, {method: "POST", body: JSON.stringify(data), onProgressChange})
			.then(res => this.handleAjaxResponse(res));
	}
}
