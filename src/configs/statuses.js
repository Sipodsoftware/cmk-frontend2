export default [
	{id: "Ordered", name: "Ordered"},
	{id: "In production", name: "In production"},
	{id: "Ready to send", name: "Ready to send"},
	{id: "Sent", name: "Sent"}
]