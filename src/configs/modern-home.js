export default {
	caseImages: [
		{url: "./assets/iphone3D/caseImages/family.jpg", phrase: "Take a family picture wherever you go."},
		{url: "./assets/iphone3D/caseImages/pattern.jpg", phrase: "Customize the case with a unique pattern."},
		{url: "./assets/iphone3D/caseImages/logo.jpg", phrase: "Create your own logo."},
		{url: "./assets/iphone3D/caseImages/flag.png", phrase: "Show everyone around that you are a proud citizen."}
	]
}
