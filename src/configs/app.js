export default {
  isRegistrationPageVisible: true,
  settings: [
    { labelText: 'Show login page', name: 'isRegistrationPageVisible', value: true, inputType: 'checkBox'}
    //{ labelText: 'Test settings', name: 'testSettings', value: 'Test value', inputType: 'textBox'},
    //{ labelText: 'Test', name: 'Test', value: '#ACACAC', inputType: 'color'},
    //{ labelText: 'Test', name: 'Test', value: 2, inputType: 'comboBox', items: [{name: 'Prvi item', value: 1}, {name: 'Drugi item', value: 2}, {name: 'Treci item', value: 23}]},
    //{ labelText: 'Test2', name: 'Test2', value: 3, inputType: 'comboBox', items: [{name: 'First item', value: 1}, {name: 'Second item', value: 2}, {name: 'Third item', value: 3}]},
    //{ labelText: 'Test2 settings', name: 'test2Settings', value: 'Test2 value', inputType: 'textBox'}
  ]
}
