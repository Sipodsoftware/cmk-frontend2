import {bindable} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {DataService} from './../../data/data-service.js';
import {Router} from "aurelia-router";

@inject(DataService, Router)
export class ProductRevealCard {

  @bindable productId;
  constructor(dataService, router) {
    this.dataService = dataService;
    this.product = {};
    this.router = router;
  }

  attached() {
    this.dataService.getProduct(this.productId).then(data => this.product = data);
  }

  productDetails() {
    // product id needed
    this.router.navigateToRoute('ProductDetails', {id: this.product.id});
  }

  addToCard() {
    this.router.navigateToRoute('ShoppingCart');
  }
}
