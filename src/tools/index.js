import {inject} from "aurelia-framework";

import {FileReaderTools} from "./file-reader-tools";
import {ImageTools} from "./image-tools";
import {ObjectTools} from "./object-tools";
import {TypeChecker} from "./type-checker";
import {ProductDesignerTools} from "./product-designer-tools";
import {DeviceTools} from "./device-tools";

export class Tools {
	fileReader;
	image;
	object;
	typeChecker;
	device;
	constructor() {
		this.fileReader = new FileReaderTools();
		this.image = new ImageTools();
		this.object = new ObjectTools();
		this.typeChecker = new TypeChecker();
		this.productDesigner = new ProductDesignerTools();
		this.device = new DeviceTools();
	}
}