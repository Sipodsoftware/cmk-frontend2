export class TypeChecker {
	isFileList(val) {
		if (val) {
			return val.constructor === window.FileList;
		}
	}

	isFile(val) {
		if (val) {
			return val.constructor === window.File;
		}
	}
}