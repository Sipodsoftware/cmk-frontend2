import {inject} from "aurelia-framework";

import {PrefixBase64ValueConverter} from "../value-converters/prefix-base64";

export class ImageTools {
	constructor() {
		this.prefixBase64 = new PrefixBase64ValueConverter();
	}

	extractBase64FromSrc(val) {
		if (val) {
			return val.split(",")[1];
		}
	}

	downloadBase64Image(base64String, filename) {
		var link = document.createElement('a');

		base64String = this.prefixBase64.toView(base64String);

		if (typeof link.download === 'string') {
			link.href = base64String;
			link.download = filename;

			document.body.appendChild(link);

			link.click();

			document.body.removeChild(link);
		} else {
			window.open(base64String);
		}
	}
}