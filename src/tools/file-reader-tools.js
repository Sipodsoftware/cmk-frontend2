import {inject} from "aurelia-framework";

import {TypeChecker} from "./type-checker";
import {ImageTools} from "./image-tools"

@inject(TypeChecker, ImageTools)
export class FileReaderTools {
	constructor(typeChecker, imageTools) {
		this.typeChecker = typeChecker;
		this.imageTools = imageTools;
	}

	convertFileToBase64(fileList) {
		return new Promise((resolve, reject) => {
			var reader;

			if (this.typeChecker.isFileList(fileList)) {
				reader = new window.FileReader;
				reader.readAsDataURL(fileList[0]);
				reader.onloadend = () => {
					resolve(this.imageTools.extractBase64FromSrc(reader.result));
				};
			} else {
				resolve(fileList);
			}
		});
	}
}