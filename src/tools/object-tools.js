export class ObjectTools {
	enableOneDisableRest(obj, propName) {
		Object.keys(obj).forEach(objPropName => {
			obj[objPropName] = objPropName === propName;
		});
	}
}