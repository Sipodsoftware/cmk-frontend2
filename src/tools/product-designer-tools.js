export class ProductDesignerTools {
	replaceImage(instance, oldElement, newElement) {
		if (oldElement) {
			instance.removeElement(oldElement);
		}

		if (newElement && newElement.source) {
			instance.addElement("image", newElement.source, newElement.title, newElement.params);
		}
	}
}