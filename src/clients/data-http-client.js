import {inject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";

import {UserState} from "./../states/user";
import {ApplicationState} from "./../states/app";
import clientsConfig from "./../configs/clients";

@inject(UserState, ApplicationState)
export class DataHttpClient extends HttpClient {
	constructor(userState, appState) {
		super();
		this.configure(config => {
			config
				.withBaseUrl(clientsConfig.baseURL)
				.withDefaults({
					headers: {
						"Accept": "application/json",
						"Content-Type": "application/json; charset=utf-8"
					}
				})
				.withInterceptor({
					request(request) {
						if (appState.interceptWithLoadingSpinner) {
							appState.showLoadingSpinner();
						}
						if (userState.token) {
							request.headers.set("Authorization", `Bearer ${userState.token}`);
						}
						return request;
					},
					response(response, request) {
						if (appState.interceptWithLoadingSpinner) {
							appState.hideLoadingSpinner();
						}
						return response;
					}
				});
		});
	}
}