import clientsConfig from "../configs/clients";

export class AjaxClient {
	
	fetch(url, options) {
		return new Promise((resolve, reject) => {
			var ajaxOptions = {
				type: options.method,
				url: `${options.baseURL || ajaxDefault.baseURL}${url}`,
				contentType: options.contentType || ajaxDefault.contentType,
				dataType: options.dataType || ajaxDefault.dataType,
				data: options.body,
				success: data => resolve(data),
				error: err => reject(err)
			};

			if (options.onProgressChange) {
				ajaxOptions.xhr = progressFunction(options.onProgressChange);
			}

			$.ajax(ajaxOptions);
		});
	}
}

var progressFunction = callback => {
	return function () {
		var xhr = new window.XMLHttpRequest();

		xhr.upload.addEventListener("progress", event => {
			callback(event);
		}, false);

		return xhr;
	}
};

var ajaxDefault = {
	baseURL: clientsConfig.baseURL,
	contentType: "application/json; charset=UTF-8",
	dataType: "json"
};