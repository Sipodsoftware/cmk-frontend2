import {inject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";

import clientsConfig from "./../configs/clients";

export class AuthHttpClient extends HttpClient {
	constructor() {
		super();
		this.configure(config => {
			config
				.withBaseUrl(clientsConfig.baseURL)
				.withDefaults({
					headers: {
						"Accept": "application/json; charset=utf-8",
						"Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
					}
				});
		});
	}
}